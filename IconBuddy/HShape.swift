//
//  HShape.swift
//  IconBuddy
//
//  Created by Paul,David on 11/23/21.
//

import SwiftUI

/*
 from 1024

top line: 255
h bar top: 484
h bar bottom 588
bottom 768

top left corner 192
bottom left corner 223

left inner column 439 436 435 431
right inner column 583 586 588 591

top right corner: 831
bottom left corner: 801
 */

let topY: CGFloat = 0.25
let hBarTopY: CGFloat = 0.47266
let hBarBottomY: CGFloat = 0.57422
let bottomY: CGFloat = 0.75

let topCornerX: CGFloat = 0.1875
let bottomCornerX: CGFloat = 0.21777
let topInnerX: CGFloat = 0.42871
let barTopX: CGFloat = 0.42578
let barBottomX: CGFloat = 0.42480
let bottomInnerX: CGFloat = 0.42090

struct HShape: Shape {
    func path(in rect: CGRect) -> Path {
        Path { path in
            path.move(to: CGPoint(x: topCornerX * rect.width, y: topY * rect.height))
            path.addLine(to: CGPoint(x: bottomCornerX * rect.width, y: bottomY * rect.height))
            path.addLine(to: CGPoint(x: bottomInnerX * rect.width, y: bottomY * rect.height))
            path.addLine(to: CGPoint(x: barBottomX * rect.width, y: hBarBottomY * rect.height))
            path.addLine(to: CGPoint(x: (1-barBottomX) * rect.width, y: hBarBottomY * rect.height))
            path.addLine(to: CGPoint(x: (1-bottomInnerX) * rect.width, y: bottomY * rect.height))
            path.addLine(to: CGPoint(x: (1-bottomCornerX) * rect.width, y: bottomY * rect.height))
            path.addLine(to: CGPoint(x: (1-topCornerX) * rect.width, y: topY * rect.height))
            path.addLine(to: CGPoint(x: (1-topInnerX) * rect.width, y: topY * rect.height))
            path.addLine(to: CGPoint(x: (1-barTopX) * rect.width, y: hBarTopY * rect.height))
            path.addLine(to: CGPoint(x: barTopX * rect.width, y: hBarTopY * rect.height))
            path.addLine(to: CGPoint(x: topInnerX * rect.width, y: topY * rect.height))
            path.addLine(to: CGPoint(x: topCornerX * rect.width, y: topY * rect.height))
        }
    }
}

struct HShape_Previews: PreviewProvider {
    static var previews: some View {
        HShape().previewLayout(.fixed(width: 96, height: 96))
    }
}
