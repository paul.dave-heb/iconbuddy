//
//  IconBuddyApp.swift
//  IconBuddy
//
//  Created by Paul,David on 11/22/21.
//

import SwiftUI

@main
struct IconBuddyApp: App {
    var body: some Scene {
        WindowGroup {
            Text("This app does nothing.  Run tests to generate images")
        }
    }
}
