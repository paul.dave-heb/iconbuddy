//
//  LaunchView.swift
//  IconBuddy
//
//  Created by Paul,David on 11/26/21.
//

import SwiftUI

struct LaunchView: View {
    var body: some View {
        ZStack(alignment: .top) {
            Grid(columnCount: 2)
                .stroke(.white).opacity(0.6)
                .background(Color.blue)
                .frame(height: 128.0)
            LinearGradient(colors: [.black.opacity(0.4), .clear], startPoint: .top, endPoint: .bottom)
                .frame(height: 12.0)
        }
    }
}

struct LaunchView_Previews: PreviewProvider {
    static var previews: some View {
        LaunchView().frame(width: 64.0).previewLayout(.sizeThatFits)
    }
}
