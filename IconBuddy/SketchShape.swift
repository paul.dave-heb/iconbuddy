//
//  SketchShape.swift
//  IconBuddy
//
//  Created by Paul,David on 11/23/21.
//

import SwiftUI

struct SketchShape: Shape {
    let stride: CGFloat

    func path(in rect: CGRect) -> Path {
        var x = self.stride
        let length = max(rect.size.height, rect.size.width)

        return Path { path in
            while x < rect.maxX * 2 {
                path.move(to: CGPoint(x: x, y: rect.minY))
                path.addLine(to: CGPoint(x: x - length, y: rect.minY + length))
                x += stride
            }
        }
    }
}

struct SketchShape_Previews: PreviewProvider {
    static var previews: some View {
        SketchShape(stride: 8.0)
            .stroke(.red)
            .mask(Circle())
            .previewLayout(.fixed(width: 180, height: 180))
    }
}
