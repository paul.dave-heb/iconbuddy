//
//  Grid.swift
//  IconBuddy
//
//  Created by Paul,David on 11/26/21.
//

import SwiftUI

struct Grid: Shape {
    let columnCount: Int

    func path(in rect: CGRect) -> Path {
        let stride = rect.width / CGFloat(columnCount)

        return Path { path in
            var x = rect.minX
            while x < rect.maxX {
                path.move(to: .init(x: x, y: rect.minY))
                path.addLine(to: .init(x: x, y: rect.maxY))
                x += stride
            }

            var y = rect.minY
            while y < rect.maxY {
                path.move(to: .init(x: rect.minX, y: y))
                path.addLine(to: .init(x: rect.maxX, y: y))
                y += stride
            }
        }
    }
}

struct Grid_Previews: PreviewProvider {
    static var previews: some View {
        Grid(columnCount: 7)
            .stroke(.yellow)
            .background(.black)
            .previewLayout(.fixed(width: 120, height: 120))
    }
}
