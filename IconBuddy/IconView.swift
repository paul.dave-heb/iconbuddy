//
//  IconView.swift
//  IconBuddy
//
//  Created by Paul,David on 11/22/21.
//

import SwiftUI

extension Color {
    static var blueprintLight: Color { Color(red: 72/255, green: 159/255, blue: 230/255) }
    static var blueprintDark: Color { Color(red: 46/255, green: 109/255, blue: 230/255) }
}

struct WidthKey: PreferenceKey {
    static var defaultValue: CGFloat = 10
    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        value = max(value, nextValue())
    }
}

struct IconView: View {
    @State var width: CGFloat = WidthKey.defaultValue

    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            LinearGradient(colors: [Color.blueprintLight, Color.blueprintDark], startPoint: .top, endPoint: .bottom)

            if width > 44 {
                Grid(columnCount: 5)
                    .stroke(.white.opacity(0.4))
                    .border(.white.opacity(0.4), width: 1.0)
            }

            if width > 64 {
                Circle()
                    .stroke(style: StrokeStyle(lineWidth: 1, dash: [8, 2, 4]))
                    .foregroundColor(.white.opacity(0.6))
                    .padding(4)
            }

            if width > 44 {
            SketchShape(stride: 5)
                .stroke(.white)
                .clipShape(HShape())
                HShape().stroke(.white)
            } else {
                HShape().fill(.white)
            }

            if width > 128 {
                Grid(columnCount: 10).stroke(.white.opacity(0.2))

                Text("DEV APP v.1.x Build A289FHZ7 (REV A)")
                    .font(Font.custom("Courier", size: 16))
                    .foregroundColor(.white).opacity(0.8)
                    .padding()
            }
        }
        .background(GeometryReader { reader in
            Color.clear.preference(key: WidthKey.self, value: reader.size.width)
        }).onPreferenceChange(WidthKey.self, perform: {
            width = $0
        })
    }
}

struct IconView_Previews: PreviewProvider {
    static var previews: some View {
        IconView()
            .previewLayout(.fixed(width: 20, height: 20))
            .previewDisplayName("Notification")
        IconView()
            .previewLayout(.fixed(width: 60, height: 60))
            .previewDisplayName("iPhoneApp")
        IconView()
            .previewLayout(.fixed(width: 76, height: 76))
            .previewDisplayName("iPadApp")
        IconView()
            .previewLayout(.fixed(width: 512, height: 512))
            .previewDisplayName("AppStore")
    }
}
