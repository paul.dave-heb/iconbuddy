//
//  IconBuddyTests.swift
//  IconBuddyTests
//
//  Created by Paul,David on 11/23/21.
//

@testable import IconBuddy
import XCTest
import SwiftUI

class IconBuddyTests: XCTestCase {
    /// To generate icons for all possible screen scales, run this test on simulator devices that match the scales required.
    /// (Devices with notches seem to have some issues, use iPhone 8 and 8 Plus)
    func test_generateIcons() throws {
        let lengths: [CGFloat] = [20, 29, 40, 60, 76, 83.5]

        for length in lengths {
            let targetSize = CGSize(width: length, height: length)
            let hostingController = UIHostingController(rootView: IconView()
                                                            .frame(width: length, height: length).edgesIgnoringSafeArea(.all))


            hostingController.view.frame = CGRect(origin: .zero, size: targetSize)
            let renderer = UIGraphicsImageRenderer(size: targetSize)
            let image = renderer.image { _ in
                hostingController.view.drawHierarchy(in: hostingController.view.frame, afterScreenUpdates: true)
            }

            let data = try XCTUnwrap(image.pngData())
            let scale = Int(UIScreen.main.scale)
            let url = URL(fileURLWithPath: "Users/p145157/icon-\(length)@\(scale)x.png")
            try data.write(to: url)
        }
    }
}
